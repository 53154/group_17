# Group_17

##### Advanced Programming Groupwork

### Project Agros
This repository contains the work developed during a two-day hackathon, in which were studied the agricultural output of several countries, hoping to contribute to a green transition. The agricultural data used is from Our World in Data and includes data on production output (crop, animal, fish), fertilizers, capital, labour used, among other factors.

On the “Class” folder, the “project_agros” script can be found with all the code used in the repository. The file has its own documentation. All the code is contained in the “Agros” class, with several methods inside. The “docs” folder contains documentation regarding Sphinx use. The “downloads” folder contains both the agricultural .csv data, used in Day 1, and the geopandas dataset, used in Day 2.

All the project findings can be found in the Showcase notebook, where the methods’ outputs are used to draw conclusions on our data, at a worldwide level, and especially focused on Portugal, Mexico and China.

### Installation
To start the project please create the same environment with the use of the environment.yml file. For that use this code in the prompt.
```bash
conda env create -f environment.yml
``` 

### Usage
Bellow there are some examples, extracted from the Showcase notebook, on how to use methods:

    # Save class Agros() in "agros"
    agros = Agros()

    # Downloads the data files and reads it into DataFrames
    agros.load_data()

    # Plots scatter plot of the output_quantity by the fertalizer_quantity, of all the countries in 2019, using log scales
    agros.gapminder(2019,True,True)

    # Returns a list of all the unique countries
    agros.countries_list()

    # Plots area chart with crop, animals and fish outputs of a certain country over time
    agros.plot_area_chart(country, False)

    # Plots linear chart of outputs of a certain country over time
    agros.display_output(countries)

    # Creates new data frame and plots a correlation matrix
    agros.correlation()
    
    # Merges datasets and plots a choropleth with the Total Factory Productivity of 2000
    agros.choropleth(2000)

    # Takes up to three countries and plots the Total Factor Productivity, by year, with an ARIMA prediction up to the year 2050
    agros.predictor(countries)

### Code Style
PEP8 compliant

### Authors

- Bernardo Manarte - 55810 - 55810@novasbe.pt

- Leonor Pereira - 48778 - 48778@novasbe.pt

- Marouan Kamoun - 53833  - 53833@novasbe.pt

- Rodrigo Simões - 53154 - 53154@novasbe.pt


### License
This project is licensed under the GNU General Public License v3.0.
(GNU)
